# NWChem

[NWChem](https://nwchemgit.github.io) is an open-source
quantum chemistry code developed by the [Environmental Molecular Sciences
Laboratory](https://www.emsl.pnnl.gov/emslweb/) at [Pacific Northwest National
Laboratory](https://www.pnnl.gov/). The code is hosted on a [GitHub
repository](https://github.com/nwchemgit/nwchem) and
[official documentation is provided](https://nwchemgit.github.io/Home.html).

## Using NWChem at NERSC

### Option \#1: Shifter images for release 7.0.2 (new)

The following scripts use an image created from the 7.0.2 NWChem release
using the following
[Dockerfile](https://github.com/nwchemgit/nwchem-dockerfiles/blob/master/nwchem-702.mpipr.nersc/Dockerfile)
to run NWChem on Cori's Haswell or KNL partitions via [Shifter](../../development/shifter/index.md).

#### Slurm script for NWChem Shifter image on Cori Haswell partition  

```bash
#!/bin/bash

#SBATCH -C haswell
#SBATCH -t 1:00:00
#SBATCH -q regular
#SBATCH -N 2
#SBATCH --ntasks-per-node=32
#SBATCH --cpus-per-task=1
#SBATCH -J my_nwchem_job
#SBATCH -o my_nwchem_job.%j.out
#SBATCH -e my_nwchem_job.%j.err
#SBATCH --image=ghcr.io/nwchemgit/nwchem-702.mpipr.nersc:latest
module swap craype-{${CRAY_CPU_TARGET},haswell}
export OMP_NUM_THREADS=1
export MPICH_GNI_MAX_EAGER_MSG_SIZE=131026
export MPICH_GNI_NUM_BUFS=80
export MPICH_GNI_NDREG_MAXSIZE=16777216
export MPICH_GNI_MBOX_PLACEMENT=nic
export MPICH_GNI_RDMA_THRESHOLD=65536
export COMEX_MAX_NB_OUTSTANDING=6
srun -N $SLURM_NNODES --cpu-bind=cores shifter nwchem input.nw
```

#### Slurm script for NWChem Shifter image on Cori KNL partition  

```bash
#!/bin/bash

#SBATCH -C knl
#SBATCH -t 1:00:00
#SBATCH -q regular
#SBATCH -N 2
#SBATCH --ntasks-per-node=64
#SBATCH --cpus-per-task=4
#SBATCH -J my_nwchem_job
#SBATCH -o my_nwchem_job.%j.out
#SBATCH -e my_nwchem_job.%j.err
#SBATCH --image=ghcr.io/nwchemgit/nwchem-702.mpipr.nersc:latest
module swap craype-{${CRAY_CPU_TARGET},mic-knl}
export OMP_NUM_THREADS=1
export MPICH_GNI_MAX_EAGER_MSG_SIZE=131026
export MPICH_GNI_NUM_BUFS=80
export MPICH_GNI_NDREG_MAXSIZE=16777216
export MPICH_GNI_MBOX_PLACEMENT=nic
export MPICH_GNI_RDMA_THRESHOLD=65536
export COMEX_MAX_NB_OUTSTANDING=6
srun -N $SLURM_NNODES --cpu-bind=cores shifter nwchem input.nw
```

#### Slurm script for NWChem Shifter image on Perlmutter CPUs

```bash
#!/bin/bash
#SBATCH -C gpu
#SBATCH -t 1:00:00
#SBATCH -N 2
#SBATCH --ntasks-per-node=32
#SBATCH --cpus-per-task=4
#SBATCH -J my_nwchem_job
#SBATCH -o my_nwchem_job.%j.out
#SBATCH -e my_nwchem_job.%j.err
#SBATCH --image=ghcr.io/nwchemgit/nwchem-702.mpipr.nersc:latest
export OMP_NUM_THREADS=2
export OMP_PROC_BIND=spread
export MPICH_GNI_MAX_EAGER_MSG_SIZE=131026
export MPICH_GNI_NUM_BUFS=80
export MPICH_GNI_NDREG_MAXSIZE=16777216
export MPICH_GNI_MBOX_PLACEMENT=nic
export MPICH_GNI_RDMA_THRESHOLD=65536
export COMEX_MAX_NB_OUTSTANDING=6
srun -N $SLURM_NNODES --cpu-bind=cores shifter nwchem input.nw
```

### Option \#2: Shifter images for the master branch (new)

The following scripts use an image created from the `master` branch
of the NWChem github repository using the following
[Dockerfile](https://github.com/nwchemgit/nwchem-dockerfiles/blob/master/nwchem-dev.mpipr.nersc/Dockerfile)
to run NWChem on Cori's Haswell or KNL partitions via [Shifter](../../development/shifter/index.md).

#### Slurm script for NWChem Shifter image on Cori Haswell partition

```bash
#!/bin/bash

#SBATCH -C haswell
#SBATCH -t 1:00:00
#SBATCH -q regular
#SBATCH -N 2
#SBATCH --ntasks-per-node=32
#SBATCH --cpus-per-task=1
#SBATCH -J my_nwchem_job
#SBATCH -o my_nwchem_job.%j.out
#SBATCH -e my_nwchem_job.%j.err
#SBATCH --image=ghcr.io/nwchemgit/nwchem-dev.mpipr.nersc:latest
module swap craype-{${CRAY_CPU_TARGET},haswell}
export OMP_NUM_THREADS=1
export MPICH_GNI_MAX_EAGER_MSG_SIZE=131026
export MPICH_GNI_NUM_BUFS=80
export MPICH_GNI_NDREG_MAXSIZE=16777216
export MPICH_GNI_MBOX_PLACEMENT=nic
export MPICH_GNI_RDMA_THRESHOLD=65536
export COMEX_MAX_NB_OUTSTANDING=6
srun -N $SLURM_NNODES --cpu-bind=cores shifter nwchem input.nw
```

#### Slurm script for NWChem Shifter image on Cori KNL partition  

```bash
#!/bin/bash

#SBATCH -C knl
#SBATCH -t 1:00:00
#SBATCH -q regular
#SBATCH -N 2
#SBATCH --ntasks-per-node=64
#SBATCH --cpus-per-task=4
#SBATCH -J my_nwchem_job
#SBATCH -o my_nwchem_job.%j.out
#SBATCH -e my_nwchem_job.%j.err
#SBATCH --image=ghcr.io/nwchemgit/nwchem-dev.mpipr.nersc:latest
module swap craype-{${CRAY_CPU_TARGET},mic-knl}
export OMP_NUM_THREADS=1
export MPICH_GNI_MAX_EAGER_MSG_SIZE=131026
export MPICH_GNI_NUM_BUFS=80
export MPICH_GNI_NDREG_MAXSIZE=16777216
export MPICH_GNI_MBOX_PLACEMENT=nic
export MPICH_GNI_RDMA_THRESHOLD=65536
export COMEX_MAX_NB_OUTSTANDING=6
srun -N $SLURM_NNODES --cpu-bind=cores shifter nwchem input.nw
```

#### Slurm script for NWChem Shifter image on Perlmutter CPUs

```bash
#!/bin/bash
#SBATCH -C gpu
#SBATCH -t 1:00:00
#SBATCH -N 2
#SBATCH --ntasks-per-node=32
#SBATCH --cpus-per-task=4
#SBATCH -J my_nwchem_job
#SBATCH -o my_nwchem_job.%j.out
#SBATCH -e my_nwchem_job.%j.err
#SBATCH --image=ghcr.io/nwchemgit/nwchem-dev.mpipr.nersc:latest
export OMP_NUM_THREADS=2
export OMP_PROC_BIND=spread
export MPICH_GNI_MAX_EAGER_MSG_SIZE=131026
export MPICH_GNI_NUM_BUFS=80
export MPICH_GNI_NDREG_MAXSIZE=16777216
export MPICH_GNI_MBOX_PLACEMENT=nic
export MPICH_GNI_RDMA_THRESHOLD=65536
export COMEX_MAX_NB_OUTSTANDING=6
srun -N $SLURM_NNODES --cpu-bind=cores shifter nwchem input.nw
```

# Project File System 

The Project File System was replaced by the [Community File
System](../community) on January 17, 2020. The path
/global/project/projectdirs/<your_repo> will continue to work until
Cori is retired.

# NERSC Supported Software Build Resources

Files and instructions which can be used to reproduce builds of supported
software at NERSC will be placed in this location as we progress through
transition to the new software policy.

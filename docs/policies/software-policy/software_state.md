# NERSC Supported Software Status List

Information about software support and status will be placed in this location
as we transition to the new software policy and that information becomes
available.

In the meantime, all NERSC support activities will continue as normal during
the year. We will not be decreasing support from what we have been providing
informally, nor will we be moving any software currently available on
production systems without at least six months notice.

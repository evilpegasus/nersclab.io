#!/bin/bash 
#SBATCH -J test
#SBATCH -q regular
#SBATCH -N 1 
#SBATCH -C knl
#SBATCH -t 48:00:00
#SBATCH -o %x-%j.out
#SBATCH -e %x-%j.err

#user setting
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export OMP_NUM_THREADS=64

./a.out


